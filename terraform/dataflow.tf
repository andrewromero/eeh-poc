resource "google_dataflow_job" "stream_quarterly_summaries" {
  name                  = "dataflow-stream-quarterly-summaries-bq"
  template_gcs_path     = "gs://dataflow-templates/latest/PubSub_Subscription_to_BigQuery"
  temp_gcs_location     = "gs://${var.project_id}-ad-hoc/dataflow-temp"
  service_account_email = google_service_account.pubsub_consumers.email
  parameters = {
    inputSubscription                   = google_pubsub_subscription.summary_to_bq.id
    outputTableSpec                     = "${var.project_id}:sec_filings.quarterly_summary"
    javascriptTextTransformGcsPath      = "gs://eeh-poc-ad-hoc/dataflow/udf/bq-udf.js"
    javascriptTextTransformFunctionName = "transform"
  }
}

resource "google_dataflow_job" "stream_to_gcs" {
  name                  = "dataflow-stream-quarterly-summaries-gcs"
  template_gcs_path     = "gs://dataflow-templates/latest/Cloud_PubSub_to_GCS_Text"
  temp_gcs_location     = "gs://${var.project_id}-ad-hoc/dataflow-temp"
  service_account_email = google_service_account.pubsub_consumers.email
  parameters = {
    inputTopic           = google_pubsub_topic.summary_topic.id
    outputDirectory      = "gs://${var.project_id}-ad-hoc/dataflow/output"
    outputFilenamePrefix = "output-"
    outputFilenameSuffix = ".json"
  }
}


resource "google_dataflow_job" "stream_to_cbt" {
  name                  = "dataflow-stream-quarterly-summaries-cbt"
  template_gcs_path     = "gs://eeh-poc-ad-hoc/dataflow/templates/pubsub-to-bigtable.json"
  temp_gcs_location     = "gs://${var.project_id}-ad-hoc/dataflow-temp"
  service_account_email = google_service_account.pubsub_consumers.email
  additional_experiments = [
    "beam_fn_api",
    "enable_streaming_engine",
    "enable_windmill_service",
    "use_multiple_sdk_containers",
    "use_portable_job_submission",
    "use_runner_v2",
    "use_unified_worker",
  ]
}
