resource "google_bigquery_dataset" "sec_filings" {
  dataset_id                 = "sec_filings"
  location                   = "US"
  delete_contents_on_destroy = true
}


resource "google_bigquery_table" "quarterly_summary" {
  dataset_id          = google_bigquery_dataset.sec_filings.dataset_id
  table_id            = "quarterly_summary"
  deletion_protection = false
  schema              = <<EOF
[
    {
        "name": "synth_key",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "pub_tms",
        "type": "TIMESTAMP",
        "mode": "NULLABLE"
    },
    {
        "name": "sub_tms",
        "type": "TIMESTAMP",
        "mode": "NULLABLE"
    },
    {
        "name": "submission_number",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "company_name",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "measure_tag",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "period_end_date",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "value",
        "type": "FLOAT",
        "mode": "NULLABLE"
    },
    {
        "name": "units",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "number_of_quarters",
        "type": "INTEGER",
        "mode": "REQUIRED"
    },
    {
        "name": "version",
        "type": "STRING",
        "mode": "REQUIRED"
    },
    {
        "name": "central_index_key",
        "type": "INTEGER",
        "mode": "NULLABLE"
    },
    {
        "name": "ein",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "sic",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "fiscal_year_end",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "form",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "fiscal_year",
        "type": "INTEGER",
        "mode": "NULLABLE"
    },
    {
        "name": "fiscal_period_focus",
        "type": "STRING",
        "mode": "NULLABLE"
    },
    {
        "name": "date_filed",
        "type": "INTEGER",
        "mode": "NULLABLE"
    },
    {
        "name": "date_accepted",
        "type": "TIMESTAMP",
        "mode": "NULLABLE"
    }
]
EOF
}
