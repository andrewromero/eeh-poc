resource "google_pubsub_topic" "summary_topic" {
  name = "quarterly-summary-topic"
}

resource "google_pubsub_subscription" "summary_manual" {
  name                 = "manual-subscription"
  topic                = google_pubsub_topic.summary_topic.name
  ack_deadline_seconds = 20
}

resource "google_pubsub_subscription" "summary_to_bq" {
  name                       = "bq-subscription"
  topic                      = google_pubsub_topic.summary_topic.name
  ack_deadline_seconds       = 30
  message_retention_duration = "172800s"
  retain_acked_messages      = true
}

resource "google_pubsub_subscription" "summary_to_gcs" {
  name                 = "gcs-subscription"
  topic                = google_pubsub_topic.summary_topic.name
  ack_deadline_seconds = 30
}

resource "google_pubsub_subscription" "summary_to_cbt" {
  name                    = "qs-to-bigtable"
  topic                   = google_pubsub_topic.summary_topic.name
  ack_deadline_seconds    = 30
  enable_message_ordering = true
}

resource "google_pubsub_subscription" "cloud_run_push" {
  name  = "cloud-run-push-subscription"
  topic = google_pubsub_topic.summary_topic.name

  ack_deadline_seconds = 60

  push_config {
    push_endpoint = var.run_url
    attributes = {
      x-goog-version = "v1"
    }
    oidc_token {
      service_account_email = google_service_account.pubsub_consumers.email
    }
  }
}
