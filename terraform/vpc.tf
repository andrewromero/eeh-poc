resource "google_compute_network" "vpc_network" {
  project                 = var.project_id
  name                    = "default"
  auto_create_subnetworks = true
  mtu                     = 1460
}

resource "google_compute_firewall" "dataflow_rule" {
  name    = "allow-dataflow"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["12345", "12346"]
  }
  source_tags = ["dataflow"]
}
