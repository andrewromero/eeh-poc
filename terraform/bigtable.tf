resource "google_bigtable_instance" "pubsub_sink" {
  name                = "eeh-bigtable-instance"
  deletion_protection = false

  cluster {
    cluster_id   = "eeh-bigtable-cluster-1"
    num_nodes    = 1
    storage_type = "SSD"
    zone         = "us-central1-c"
  }

  lifecycle {
    prevent_destroy = false
  }
}

resource "google_bigtable_table" "quarterly_summary" {
  name          = "quarterly_summary"
  instance_name = google_bigtable_instance.pubsub_sink.name
  column_family {
    family = "def_family"
  }
  lifecycle {
    prevent_destroy = false
  }
}

resource "google_bigtable_table" "quarterly_summary_cr" {
  name          = "quarterly_summary_cloud_run"
  instance_name = google_bigtable_instance.pubsub_sink.name
  column_family {
    family = "def_family"
  }
  lifecycle {
    prevent_destroy = false
  }
}
