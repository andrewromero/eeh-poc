terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.19.0"
    }
  }
  backend "gcs" {}
}

provider "google" {
  project = var.project_id
  region  = "us-central1"
}



resource "google_service_account" "pubsub_consumers" {
  account_id = "pubsub-consumers"
}
resource "google_project_iam_member" "consumers_role" {
  for_each = toset([
    "roles/bigquery.admin",
    "roles/bigtable.admin",
    "roles/cloudfunctions.admin",
    "roles/dataflow.serviceAgent",
    "roles/dataflow.worker",
    "roles/pubsub.admin",
    "roles/run.admin",
    "roles/storage.admin",
  ])
  role    = each.key
  member  = "serviceAccount:${google_service_account.pubsub_consumers.email}"
  project = var.project_id
}


resource "google_storage_bucket" "ad_hoc_bucket" {
  name                        = "${var.project_id}-ad-hoc"
  location                    = "US"
  uniform_bucket_level_access = true
  force_destroy               = true
}

resource "google_storage_bucket_object" "bq_pubsub_archive" {
  name   = "cf-stage/bq-to-pubsub.zip"
  bucket = google_storage_bucket.ad_hoc_bucket.name
  source = "../functions/bq-to-pubsub.zip"
}

resource "google_cloudfunctions_function" "bq_pubsub_function" {
  name                  = "bq-to-pubsub-v3"
  runtime               = "python38"
  available_memory_mb   = 8192
  timeout               = 180
  source_archive_bucket = google_storage_bucket.ad_hoc_bucket.name
  source_archive_object = google_storage_bucket_object.bq_pubsub_archive.name
  trigger_http          = true
  entry_point           = "exec_bq_pubsub"
  ingress_settings      = "ALLOW_INTERNAL_AND_GCLB"
  service_account_email = google_service_account.pubsub_consumers.email
  environment_variables = {
    project_id = var.project_id
    topic_id   = google_pubsub_topic.summary_topic.name
  }
  depends_on = [
    google_storage_bucket_object.bq_pubsub_archive
  ]
}
