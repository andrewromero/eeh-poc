import datetime
import json
import os
import random
import string
from google.cloud import bigquery
from concurrent import futures
from google.cloud import pubsub_v1

project_id = os.environ.get('project_id')
topic_id = os.environ.get('topic_id')
letters = string.ascii_letters


client = bigquery.Client(project=project_id)
publisher = pubsub_v1.PublisherClient(
    batch_settings=pubsub_v1.types.BatchSettings(
        max_messages=1000,
        max_bytes=10024,  # 1 MB
        max_latency=1,  # 1 second
    ),
    publisher_options=pubsub_v1.types.PublisherOptions(
        enable_message_ordering=True
    )
)

def exec_bq_pubsub(request=None):
    batch_size = request.get_json(silent=True)['batch_size']
    query_job = client.query(f"""
        SELECT *
        FROM `bigquery-public-data.sec_quarterly_financials.quick_summary`
        TABLESAMPLE SYSTEM (3 PERCENT)
        order by rand()
        LIMIT {batch_size}
    """)
    results = query_job.result() # Wait for the job to complete.

    topic_path = publisher.topic_path(project_id, topic_id)
    publish_futures = []

    for rec in results:
        synth_key = ''.join(random.choice(letters) for i in range(10))
        rec = dict(rec)
        rec['synth_key'] = synth_key
        rec['pub_tms'] = datetime.datetime.utcnow()
        rec = json.dumps(rec, default=str).encode('utf-8')

        publish_future = publisher.publish(topic_path, data=rec, ordering_key=synth_key)
        # Non-blocking. Allow the publisher client to batch multiple messages.
        publish_future.add_done_callback(callback)
        publish_futures.append(publish_future)

    futures.wait(publish_futures, return_when=futures.ALL_COMPLETED)

    print(f"Published messages with batch settings to {topic_path}.")
    return f"Quarterly summary messages published with batch size of {batch_size}."


# Resolve the publish future in a separate thread.
def callback(future: pubsub_v1.publisher.futures.Future) -> None:
    message_id = future.result()
