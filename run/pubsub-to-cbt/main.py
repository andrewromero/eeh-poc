import base64
import datetime
import json
import os
from flask import Flask, request
from google.cloud import bigtable
from google.cloud.bigtable import row



app = Flask(__name__)
client = bigtable.Client()


@app.route("/", methods=["POST"])
def index():
    envelope = request.get_json()
    if not envelope:
        msg = "No Pub/Sub message received"
        print(f"error: {msg}")
        return f"Bad Request: {msg}", 400

    if not isinstance(envelope, dict) or "message" not in envelope:
        msg = "invalid Pub/Sub message format"
        print(f"error: {msg}")
        return f"Bad Request: {msg}", 400

    pubsub_message = envelope["message"]

    if isinstance(pubsub_message, dict) and "data" in pubsub_message:
        msg = base64.b64decode(pubsub_message["data"]).decode("utf-8").strip()
        msg = json.loads(msg)

        table = client.instance('eeh-bigtable-instance').table('quarterly_summary_cloud_run')
        direct = table.direct_row(
            row_key=msg['synth_key']
        )
        for key, val in msg.items():
            direct.set_cell(
                'def_family',
                str(key).encode('utf-8'),
                str(val).encode('utf-8'),
                datetime.datetime.utcnow()
            )
        direct.commit()
    return ("", 204)


if __name__ == "__main__":
    PORT = int(os.getenv("PORT")) if os.getenv("PORT") else 8080
    # used when running locally only
    app.run(host="127.0.0.1", port=PORT, debug=True)
