# EEH POC

## Tasks for bootstrapping this environment in GCP
* Create a GCS bucket with a name of your choosing to store the Terraform state file
* Add this bucket name to the `terraform/backend.conf` file
* Deploy the Cloud Run service (details below)
* Update vars.tfvars with the required parameters
* Apply the Terraform config

## Deploying resources with Terraform
```bash
gcloud auth application-default login
cd terraform/
terraform init -backend-config=backend.conf
terraform apply -var-file="vars.tfvars"
```

## Deploying the Cloud Run service
Command will automatically build the container image and deploy it to Cloud Run. Update service account email as needed.

```
gcloud run deploy pubsub-to-cbt --source=run/pubsub-to-cbt/ --region=us-central1 --max-instances=20 --service-account=pubsub-consumers@eeh-poc.iam.gserviceaccount.com
```

## Other notes
To ensure Cloud Function files are properly zipped (required for deployment via TF if Function code is updated):
`zip -rj functions/bq-to-pubsub.zip functions/bq-to-pubsub/*`

To manually invoke Cloud Function:
`gcloud functions call bq-to-pubsub-v3 --data '{"batch_size":"100000"}'`

To run the Cloud Run service locally, build a Python virutal environment and install dependencies from the `run/pubsub-to-cbt/local-req.txt` file. Then:
```
export FLASK_APP=main
flask run
```

Reading Bigtable via the CLI:
```bash
cbt -instance eeh-bigtable-instance -project eeh-poc read quarterly_summary
cbt -instance eeh-bigtable-instance -project eeh-poc count quarterly_summary
```

### Datasets used
* bigquery-public-data.sec_quarterly_financials.quick_summary --> quarterly_summary
    * 16.56 GB, ~89.6M records
