import apache_beam as beam
from apache_beam.io.gcp.bigtableio import WriteToBigTable
from apache_beam.options import pipeline_options


proj_id = 'eeh-poc'
region = 'us-central1'
instance_id = 'eeh-bigtable-instance'
table_id = 'quarterly_summary'

gcs_bucket = 'eeh-poc-ad-hoc'
subscription = f'projects/{proj_id}/subscriptions/qs-to-bigtable'


options = pipeline_options.PipelineOptions(
    max_num_workers=8,
    project = proj_id,
    region = region,
    service_account_email='pubsub-consumers@eeh-poc.iam.gserviceaccount.com',
    staging_location = f'gs://{gcs_bucket}/dataflow/stage',
    streaming=True,
    temp_location = f'gs://{gcs_bucket}/dataflow/temp',
)

p = beam.Pipeline(options=options)


def create_row(ele):
    from google.cloud.bigtable import row
    import datetime
    import json
    ele = json.loads(ele)
    direct_row = row.DirectRow(
        row_key=ele['submission_number'] + '#' + ele['measure_tag']
    )
    for key, val in ele.items():
        direct_row.set_cell(
            'def_family',
            str(key).encode('utf-8'),
            str(val).encode('utf-8'),
            datetime.datetime.utcnow()
        )
    return direct_row


def run():
    data = (p |
        'Read PubSub' >> beam.io.ReadFromPubSub(subscription=subscription) |
        'Convert to BT row' >> beam.Map(create_row) |
        'Write To Bigtable' >> WriteToBigTable(
            project_id=proj_id,
            instance_id=instance_id,
            table_id=table_id
        )
    )
    logs = p.run()
    return

if __name__ == "__main__":
    run()
